package com.array;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] workedHoursPerDay = new int[7];
        for (int i = 0; i < workedHoursPerDay.length; i++) {
            workedHoursPerDay[i] = scanner.nextInt();
        }
        System.out.println("Weekly Salary " + Salary(workedHoursPerDay));
    }

    private static double Salary(int[] workedHoursPerDay) {
        int totalSalary = 0;
        int amountFromMondayToFriday = 0;
        int mondayToFridayHours = 0;

        for (int i = 1; i < 7 - 1; i++) {
            mondayToFridayHours += workedHoursPerDay[i];
            amountFromMondayToFriday += (workedHoursPerDay[i] * 100) + (workedHoursPerDay[i] > 8 ? (workedHoursPerDay[i] - 8) * 15 : 0);
        }
        totalSalary += amountFromMondayToFriday;

        int saturday = workedHoursPerDay[6] * 100;
        int sunday = workedHoursPerDay[0] * 100;
        totalSalary += saturday + sunday;

        if (mondayToFridayHours < 40) {
            totalSalary += (saturday * .25) + (sunday * .50);
        }

        return totalSalary;
    }
}

/*
Input
0
8
8
8
10
6
0

Output
4030
 */
